@extends('layouts.app')
@section('content')
<div class="card">
    <div class="card-header">
        {{ $modelName }}
    </div>

    <div class="card-body">
        @include('layouts.flash-session')

        <table class="table table-striped table-hover table-bordered">
          <thead>
            <tr>
                <th class="">Transaction ID</th>
                <th class="">Date</th>
                <th class="">Account Number</th>
                <th class="">Account Name</th>
                <th class="">Description</th>
                <th class="">Debit</th>
                <th class="">Credit</th>
                <th class="text-center">Actions</th>
            </tr>
          </thead>
          <tbody>
                @if ($models->count() == 0)
                    <tr>
                        <td colspan="8" class="text-center">No available {{ $title }}</td>
                    </tr>    
                @else
                    @foreach ($models as $model)
                        <tr>
                            <td>
                                {{ $model->trans_id }}
                            </td>
                            <td>
                                {{ $model->created_at->format('D, d-m-Y h:i') }}
                            </td>
                            <td>
                                {{ $model->bankAccount->number }}
                            </td>
                            <td>
                                {{ $model->bankAccount->name }}
                            </td>
                            <td>
                                {{ $model->description }}
                            </td>
                            <td class="text-right">
                                {!! ($model->type == $model::BANK_TRANS_TY_DB) ? $model->amountFormat : '0' !!}
                            </td>
                            <td class="text-right">
                                {!! ($model->type == $model::BANK_TRANS_TY_CR) ? $model->amountFormat : '0' !!}
                            </td>
                            <td>
                                {{-- <a href="{{route('bank.trans.show', $model->getKey())}}" class="btn btn-primary btn-sm"><em class="fa fa-folder"></em> View </a> --}}
                                <a href="#" class="btn btn-danger btn-sm btn-delete" data-id='{{ $model->getKey() }}' data-title='{{ $model->trans_id }}'><em class="fa fa-trash"></em> Delete </a>
                            </td>
                        </tr>
                    @endforeach
                @endif
          </tbody>
        </table>
        {{ $models->onEachSide(1)->links() }}
    </div>
</div>
@include('bank.trans._delete')
@endsection