<ul class="navbar-nav mr-auto">
    @php
        $menu = [
            'bank.lists'    => 'Bank Lists',
            'bank.users'    => 'Bank Users',
            'bank.accounts' => 'Bank Accounts',
            'bank.trans'    => 'Transactions',
            'bank.logs'     => 'Bank Logs'
        ];

        $segments = str_replace(config('app.url'), '', Request::url());
    @endphp
    @foreach ($menu as $key => $item)
        <li class="nav-item">
            <a 
                class="nav-link @if (substr_count($segments, route($key.'.index')) > 0) active @endif" 
                href="{{ route($key.'.index') }}">
                {{ $item }}
            </a>
        </li>
    @endforeach
</ul>