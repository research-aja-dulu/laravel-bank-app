@if (session('status'))
    <div class="alert alert-{{ session('status') }}" role="alert">
        <strong>{{ session('head') }}</strong><br>
        {{ session('message') }}
    </div>
@endif

@if ($errors->count() > 0)
    <div class="alert alert-danger" role="alert">
        <strong>Oops! There is an error.</strong><br>
        Please check the form below again.
    </div>
@endif