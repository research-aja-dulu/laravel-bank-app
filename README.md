# README #

Laravel Bank App is dashboard panel for manage account banks (ID) based on Laravel 5.8

## What is this repository for?

* Laravel Template
* Version 1.0

### How do I get set up?

* Git clone to specific folder
* `composer install`
* Copy .env-example to .env (if no .env file before)
* Configuration .env
* Setting up token of [bot telegram](https://core.telegram.org/bots) from [@BotFather](https://t.me/BotFather)
* Setting up [AntiCaptcha](https://anti-captcha.com) key in .env file
* Create Database
* `php artisan optimize:clear`
* `mkdir app/MantoServices/Bank` (if not yet)
* `mkdir app/MantoServices/AntiCaptcha` (if not yet) (optional)
* `mkdir storage/bank/data` (if not yet)
* `sudo chown -R user:apache-user storage/*` (every server different)
* `sudo chmod -R 775 storage/*`
* `php artisan migrate --seed`


### Setting up supervisor & task scheduler

#### Installing supervisor

`sudo apt-get install supervisor`

#### Configure supervisor

* `nano /etc/supervisor/conf.d/laravel-worker.conf`
* Add this code
```bash
[program:laravel-worker]
process_name=%(program_name)s_%(process_num)02d
command=php /path-your-project/artisan queue:work --sleep=3 --tries=3
autostart=true
autorestart=true
user=bankscrape
numprocs=3
redirect_stderr=true
stdout_logfile=/path-your-log/worker.log
```
* Please check user, directory of command & directory of logfile

#### Starting supervisor

* `sudo supervisorctl reread`
* `sudo supervisorctl update`
* `sudo supervisorctl start laravel-worker:*`
* More info please check [Laravel Supervisor](https://laravel.com/docs/5.8/queues#supervisor-configuration)

#### Task scheduler

* Edit your crontab list with command: `crontab -e`
* Add this command on your crontab list
```bash
* * * * * cd /path-to-your-project && php artisan schedule:run >> /dev/null 2>&1
```

### Who do I talk to?

* Repo owner or admin
* Other community or team contact