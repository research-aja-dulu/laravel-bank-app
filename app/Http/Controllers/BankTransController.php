<?php

namespace App\Http\Controllers;

use App\BankTrans;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class BankTransController extends Controller
{
    protected $modelName = 'Bank Trans';
    protected $listStatus;

    function __construct(){
        parent::__construct();
        $this->model        = BankTrans::modelName();
        $this->listStatus   = BankTrans::listStatus();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title      = $this->title($this->modelName);
        $model      = $this->model;
        $models     = BankTrans::paginate(25);
        $modelName  = $this->modelName;

        return view('bank.trans.index', compact('title', 'modelName', 'models', 'model'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $model  = BankTrans::findOrFail($id);
        $actionName = 'deleted';
        if ( $model->delete() ) {
            Session::flash('status', 'success');
            Session::flash('head', 'Item '.$actionName.' successfully');
            Session::flash('message', $this->title($this->modelName)." Trans ID '".$model->trans_id."' ".$actionName.".");
        } else {
            Session::flash('status', 'error');
            Session::flash('head', 'Item not '.$actionName.' successfully');
            Session::flash('message', $this->title($this->modelName)." Trans ID '".$model->trans_id."' not ".$actionName.".");
        }
        return redirect()->route('bank.trans.index');
    }
}
