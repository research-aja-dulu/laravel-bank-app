<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BankAccountsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function createRules()
    {
        return [
            'bank_user'         => 'required',
            'number'            => 'required|min:3|max:191|unique:bank_accounts',
            'name'              => 'required|min:3|max:191|regex:/^[a-zA-Z0-9 ]+$/',
            'type'              => 'required|in:1,2,3',
            'daily_job_status'  => 'required|in:1,2',
            'delay_job_minutes' => 'required|numeric|min:5',
            'saldo'             => 'required|numeric',
            'status'            => 'required|in:1,2',
        ];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = $this->createRules();
        if ($this->isMethod('patch')) {
            $rules['number']      = '';
            $rules['bank_user']   = '';
        }
        return $rules;
    }

    public function attributes()
    {
        return [
            'bank_user' => 'bank user',
            'number'    => 'account number',
            'daily_job_status' => 'daily job status',
            'delay_job_minutes' => 'daily job minutes',
        ];
    }
}
