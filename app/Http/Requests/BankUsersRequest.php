<?php

namespace App\Http\Requests;

use App\Bank;
use Illuminate\Foundation\Http\FormRequest;

class BankUsersRequest extends FormRequest
{
    protected $model;

    public function __construct()
    {
        parent::__construct();
        $this->model = Bank::getBCA();
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function createRules()
    {
        $rules = [
            'username'  => 'required|min:3|max:191|unique:bank_users',
            'password'  => 'confirmed|required',
            'type'      => 'required|in:1,2',
            'corp_id'   => 'required_if:type,2|max:191|unique:bank_users',
            'bank_id'   => 'required',
            'status'    => 'required|in:1,2'
        ];

        if($this->type == 2 && $this->bank_id == $this->model->getKey() || $this->isMethod('patch')){
            $rules['api_key'] = 'required_if:bank_id,'.$this->model->getKey().'|required_if:type,2|unique:bank_users';
            $rules['api_secret'] = 'required_if:bank_id,'.$this->model->getKey().'|required_if:type,2|confirmed';
        }else{
            $rules['username'] .= "|regex:/^[a-zA-Z0-9]+$/";
        }

        return $rules;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = $this->createRules();

        if ($this->isMethod('patch')) {
            $rules['corp_id']       = $rules['corp_id'].',corp_id,'.$this->id;
            $rules['password']      = (is_null($this->password)) ? '' : $rules['password'];
            $rules['bank_id']       = '';
            $rules['type']          = '';
            $rules['username']      = '';

            $rules['api_secret']    = (is_null($this->api_secret)) ? '' : $rules['api_secret'];
        }

        return $rules;
    }

    public function attributes()
    {
        return [
            'bank_id' => 'Bank',
            'corp_id' => 'Corp ID',
            'api_key' => 'API Key',
            'api_secret' => 'API Secret',
        ];
    }

    public function messages()
    {
        return [
            'api_secret.required_if' => 'The :attribute field is required when Bank is '.$this->model->name.' and type is Corporate',
            'api_key.required_if' => 'The :attribute field is required when Bank is '.$this->model->name.' and type is Corporate'
        ];
    }
}
