<?php

namespace App\Console\Commands;

use App\BankUser;
use App\MantoServices\Bank\IbParser;
use Illuminate\Console\Command;

class ParserBank extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'parser:run';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Run all parser via cron';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $users = BankUser::whereStatus(1)->get();
        foreach ($users as $user) {
            $bank = ($user->type == 1) ? $user->bank->personal_lib : $user->bank->corporate_lib;
            foreach ($user->bankAccounts as $account) {
                $parser     = new IbParser($user, $account, -1);
                $type       = $parser->getType($account->type);
                $parser->$type($bank);
            }
        }
    }
}
