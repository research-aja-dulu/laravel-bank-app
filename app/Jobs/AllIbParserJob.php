<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\BankUser;

class AllIbParserJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $users;
    protected $time;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($time)
    {
        $this->time      = $time;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->users    = BankUser::where(['status' => 1])->get();
        
        foreach ($this->users as $user) {
            foreach ($user->bankAccounts as $account) {
                IbParserJob::dispatch($user, $account, $this->time)
                                ->delay(now()->addSecond(5));
            }
        }
    }
}
