<?php

use App\Bank;
use App\BankTransGroup;
use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Schema;

class TruncateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();
        Bank::truncate();
        BankTransGroup::truncate();
        User::truncate();
        Schema::enableForeignKeyConstraints();
    }
}
