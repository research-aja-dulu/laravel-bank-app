<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call('TruncateSeeder');
        $this->call('BankTableSeeder');
        $this->call('BankTransGroupTableSeeder');
        $this->call('UserSeeder');
    }
}
