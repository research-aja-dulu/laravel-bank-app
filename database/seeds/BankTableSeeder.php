<?php

use Illuminate\Database\Seeder;
use App\Imports\BankImport;

class BankTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Excel::import(new BankImport, public_path('docs/domestic-bank-list.xlsx'));
    }
}
