<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::firstOrCreate([
            'name'  => env('ADMIN_NAME', 'Administrator'),
            'email' => env('ADMIN_EMAIL', 'instamaxv@gmail.com'),
            'password'  => Hash::make(env('ADMIN_PASSWORD', 'adminaja')),
        ]);
    }
}
