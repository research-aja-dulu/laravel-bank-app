<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterBankTrans extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bank_trans', function (Blueprint $table) {
            $table->foreign('bank_trans_group_id')->references('id')->on('bank_trans_groups');
            $table->foreign('bank_account_id')->references('id')->on('bank_accounts');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bank_trans', function (Blueprint $table) {
            $table->dropForeign(['bank_trans_group_id']);
            $table->dropForeign(['bank_account_id']);
        });
    }
}
